\version "2.14.0"
\pointAndClickOff

\include "articulate.ly"

tempoMark = #(
	define-music-function (parser location prependText noteValue appendText) (string? string? string?) #{
		\mark \markup {
			\line { $prependText " (" \fontsize #-2 \general-align #Y #DOWN \note #$noteValue #1 $appendText ) }
		}
	#}
)

\paper {
	indent = 0
}

\header {
	title = "Final Fantasy 3 - Eternal Wind"
	composer = "Nobuo Uematsu"
	arranger = "Y.N."
    transcriber = "1Swatty"
	tagline = ""
}

global = {
	\key c \major
	\time 4/4

	\set Score.voltaSpannerDuration = #(ly:make-moment 4 4)
}

upper = \relative c' {
	\clef treble

	\tag #'sheet {
		\once \override Score.RehearsalMark #'self-alignment-X = #-1
		\tempoMark "Allegro moderato" "4" "= 120"
	}
	\tag #'midi { \tempo 4 = 120 }

	\repeat volta 2 {
		<e g d'>2.. <b' d e g>8 |
		<g b c e>2.. <g b c>8 |
		<e g d'>8 << { <dis' e>2.. } \\ { r16 <c, e>8. <c e b'>8 <c e a> r16 <c e>8. } >> |
		d16 dis32 e g16 c,8 b8 g16 a c e, b' g a8. |
		<< { <f' a d>2.. } \\ { r8 <f, a e'> r16 <f a e'>8. <f a e'> <f a e'>8 } >> <a' c e>8 |
		<< { <e f a c>2. d'4 } \\ { r8 <a, c g'> r16 <a c g'>8. <c b'>8 <e g> e <f a> } >> |
	}
	\alternative {
		{
			<< { r8 <d' e g b>8. <b d e g>8 e16 d dis32 e g16 b,8 bes16 a g } \\ { <d e g b>1 } >> |
			e16 g d32 dis e16 b g8 a'16 b e, a e g <dis a' d>8. |
		} {
			<< { r8 <d' e g b>8. <e g b d>8 <d e g b>16 r bes'32 a g16 e d b bes a } \\ { <d, e g b>1 } >> |
			g16 gis e fis d b8 a'16 gis e b' c d e fis gis |
		}
	}
	\repeat volta 2 {
		<< { <c, e a>2~<c e a>8 } \\ { r8 <f,, a e'> r16 <f a e'>8. s8 } >> <a' c e>8 <a c e> <a c g'> |
		<< { <c e a>4 } \\ { r8 <f,, a e'> } >> r16 <e'' f a c>8. c16 <e b'>8 a,16 g' b, d f |
		<< { <b, d a'>2~<b d a'>8 } \\ { r8 <g, b e> r16 <g b e>8. s8 } >> <g' b e>8 <g b e> <g b d> |
	}
	\alternative {
		{
			<< { <g b e>2 e'4 g } \\ { r8 <g,, b d e> r16 <g b d e>8. r16 e' es' d r16 d des c } >>  |
			<< { <a c g'>4~<a c g'>16 } \\ { r8 <a, c g'> s16 } >> <f' a d>8. e16 f <f d'>8 <f d'> \acciaccatura as8 <g e'> |
			<< { <a c f>4~<a c f>16 <a c f>8. } \\ { r8 <a, c d f>8 s4 } >> f'16 <a e'>8 e16 d' f, a d, |
			<< { <e gis e'>1 } \\ { r16 \stemUp { fis16 gis b a gis8 e16 <cis e b'>8. <cis e b'>16 r16 <cis e a>16 <d e gis>8 } } >> |
			<< { <gis b gis'>1 } \\ { r16 b16 c d c b8 a16 \stemUp { gis->e gis a-> e a b8-> } } >> |
		} {
			<< { <g b e>1 } \\ { r8 <g, b e> r16 <g b dis e>8. <g b d>16 r8 <g c>16 r8 <g b>16 r } >> |
			<< { <bes' d a'>2~<bes d a'>8 } \\ { r8 <f, a bes d> r16 <f a bes d>8. s8 } >> <bes' d a'>8 <bes d g> <bes d f> |
			<< { <f a e'>4~<f a e'>16 <f a d>8. } \\ { r8 <a, bes d f> s4 } >> e'16 <f as c>8 d16 d' f, a d |
			<< { <a b e>4~<a b e>16 <a' b e>8. } \\ { r8 <a,, b e> s4 } >> <a'' b d>4 <a b fis'> |
			<< { <gis b gis'>1 } \\ { r8 \stemUp { <d, fis> <b d> <fis' a> <b d> <fis a> gis32 fis e fis gis a b c } } >> |
		}
	}
	<e, g d'>2.. <b' d e g>8 |
	<g b c e>2.. <g b c>8 |
	<e g d'>8 << { <dis' e>2.. } \\ { r16 <c, e>8. <c e b'>8 <c e a> r16 <c e>8. } >> |
	d16 dis32 e g16 c,8 b8 g16 a c e, b' g a8. |
	<< { <f' a d>2.. } \\ { r8 <f, a e'> r16 <f a e'>8. <f a e'> <f a e'>8 } >> <a' c e>8 |
	<< { <e f a c>2. d'4 } \\ { r8 <a, c g'> r16 <a c g'>8. <c b'>8 <e g> e <f a> } >> |
	<< { r8 \autoBeamOff <d' e g b>8. <b d e g>8 \stemDown { e16[ d dis32 e g16] } \stemUp { b,8[ bes16 a g] } \autoBeamOn } \\ { <d e g b>1 } >> |
	e16 g d32 dis e16 b g8 a'16 <c, e b'> r8
	\tag #'sheet { \autoBeamOff <dis a' d>8. << { \override Glissando #'style = #'zigzag \hideNotes a''''16\glissando f,, \unHideNotes } \\ { r8 } >> \autoBeamOn | }
	\tag #'midi { << { <dis, a' d>8. s8 } \\ { s16. \times 3/17 { a''''16 g f e d c b a g f e d c b a g f } } >> | }
	<< { r16 \stemDown { g d e b g8 a'16 b e, a e g b8 b16~ } | b1 | } \\ { <g, e'>1 | e'16 g d e b g8 a'16 b e, a e g b8 b16 | } >>
	<b, e>16 <d g> <a d> <b e> <g b> <e g>8 <fis' a>16 <g b> e <fis a> e <e g> <g b>8 <g b>16~ |
	<e g b>16 a g <e b'>8 a16 g <dis a' d> r <dis, a' d> r <dis, a' d> r <dis' a' d>8 dis'16 |
	\once \override Score.RehearsalMark #'self-alignment-X = #-1
	\mark \markup { \line { "Meno Mosso" } }
	\tag #'midi { \tempo 4 = 80 }
	<< { <g, b e>1 } \\ { r16 \stemUp { g16 d e b g8 a'16 b e, a e g b8 b16 } } >> |
	<< { <fis a e'>1 } \\ { r16 \stemUp { fis d e b a8 a'16 b e, a e fis b8 b16 } } >> |
	<< { <g b e>1 } \\ { r16 \stemUp { g16 d e b g8 a'16 } \stemDown { b8. d16 e8. g32 a } } >> |
	\tag #'sheet { b2 \ottava #1 <d e g b d e>\arpeggio \ottava #0 | }
	\tag #'midi { b2 \set tieWaitForNote = ##t \grace { d16~ e~ g~ b~ d~ e~ } <d, e g b d e>2 }
}

lower = \relative c {
	\clef bass

	\repeat volta 2 {
		<< { r8 <e g d'>8 r16 <e g d'>8. <e g d'>8. <e g d'>16 } \\ { <c, c'>8. <c c'>16 <c c'>8. <c c'>16 r16 <c c'>8. } >> c16 g' c g |
		<< { r8 <g' b e>8 r16 <g b e>8. <g b e>8. <g b e>16 } \\ { <c,, c'>8. <c c'>16 <c c'>8. <c c'>16 r16 <c c'>8. } >> c16 c' b, b' |
		\ottava #-1
		a,8 a'16 e a,8. a'16 r a, r a' a, e' a e |
		a,8 a'16 e a,8 a' a,16 a' e a a, a' g, g' |
		<f, f'>8. <f f'>16 <f f'>8. <f f'>16 r16 <f f'>8. f16 c' f c |
		<f, f'>8. <f f'>16 <f f'>8. <f f'>16 r16 f c' f f, c' f c |
		\ottava #0
	}
	\alternative {
		{
			\ottava #-1
			e,8 e'16 b e,8 e' e, e'16 b e,8 dis' |
			e,8 e'16 b e,8 e'
			\ottava #0
			<a, a'> <a' e' g> <b, b'>16 <b b'>8 <b b'>16 |
		} {
			\ottava #-1
			e,8 e'16 b e,8 e' e, e'16 b e,8 dis' |
			e,8 e'16 b e,8 e' e, e'16 b e, e' dis e |
			\ottava #0
		}
	}
	\repeat volta 2 {
		\repeat unfold 2 {
			<f, f'>8. <f f'>16 <f f'>8. <f f'>16 f c' f f c' f c f,
		}
		<e, e'>8. <e e'>16 <e e'>8. <e e'>16 e b' e e b' e b e, |
	}
	\alternative {
		{
			<e, e'>8. <e e'>16 <e e'>8. <e e'>16 e b' e e b' e b e, |
			\ottava #-1
			\repeat unfold 2 {
				<d, d'>8. <d d'>16 <d d'>8. <d d'>16 d a' d d a' d a d, |
			}
			<cis, cis'>16
			\ottava #0
			r8 <b'' e>16 r8 <gis b>16 r8
			\ottava #-1
			<cis,, cis'>8. cis16 cis' d, d' |
			<e, e'>16 r8 <dis'e>16 r8 <dis e>16 r <e, e'>16 r8 <dis' e>16 r8 <e, e'>16 r16 |
			\ottava #0
		} {
			\ottava #-1
			<e e'>8. <e e'>16 <e e'>8. <e e'>16 e-> b' e e,-> b' e b,-> b' |
			\repeat unfold 2 {
				<bes, bes'>8. <bes bes'>16 <bes bes'>8. <bes bes'>16 bes f' bes bes f' bes f bes, |
			}
			<b, b'>8. <b b'>16 <b b'>8. <b b'>16 \repeat unfold 2 { b16 b'32 fis b,16 b' } |
			\times 4/6 { <e, e'>16 b' e e, b' e } \repeat unfold 2 { \times 4/6 { e,16 b' e e, b' e } } e,16 e' e, e' |
			\ottava #0
		}
	}
	<< { r8 <e' g d'>8 r16 <e g d'>8. <e g d'>8. <e g d'>16 } \\ { <c, c'>8. <c c'>16 <c c'>8. <c c'>16 r16 <c c'>8. } >> c16 g' c g |
	<< { r8 <g' b e>8 r16 <g b e>8. <g b e>8. <g b e>16 } \\ { <c,, c'>8. <c c'>16 <c c'>8. <c c'>16 r16 <c c'>8. } >> c16 c' b, b' |
	\ottava #-1
	a,8 a'16 e a,8. a'16 r a, r a' a, e' a e |
	a,8 a'16 e a,8 a' a,16 a' e a a, a' g, g' |
	<f, f'>8. <f f'>16 <f f'>8. <f f'>16 r16 <f f'>8. f16 c' f c |
	<f, f'>8. <f f'>16 <f f'>8. <f f'>16 r16 f c' f f, c' f c |
	e,8 e'16 b e,8 e' e, e'16 b e,8 dis' |
	e,8 e'16 b e,8 e' <a,, a'>16 r8 <b a'>16 r8 <b a'>16 r |
	\ottava #0
	\repeat unfold 4 { \times 4/6 { e'16 b' e e, b' e } } |
	\repeat unfold 4 { \times 4/6 { d,16 b' d d, b' d } } |
	\repeat unfold 4 { \times 4/6 { cis,16 a' cis cis, a' cis } } |
	\times 4/6 { c,16 g' c c, g' c } \times 2/3 { c,16 g' c } <c, c'>16 <b b'> r <b b'> r8 <b b'> <b b'> |
	<c c'>1 |
	<d d'>1 |
	<e e'>1~ |
	<e e'>1 |
}

dynamics = {
	s1*34
	s2
	s2\>
	s1\!
	s1
	s1
	^\markup { \italic { smorzando } }
	s2 s2\pp
}

pedal = {
	\set Dynamics.pedalSustainStyle = #'mixed
	s1*36
	s1
	s1\sustainOn
	s1
}

\score {
	\new PianoStaff = "PianoStaff_pf" <<
		\new Staff = "Staff_pfTreble" << \global \removeWithTag #'midi \upper >>
		\new Dynamics = "Dynamics_pf" \dynamics
		\new Staff = "Staff_pfBass" << \global \removeWithTag #'midi \lower >>
		\new Dynamics = "Pedal_pf" \pedal
	>>

	\layout {}
}

\score {
	\new PianoStaff = "PianoStaff_pf" <<
		\new Staff = "Staff_pfTreble" <<
			\global { \removeWithTag #'sheet \unfoldRepeats \articulate \upper }
		>>
		\new Staff = "Staff_pfBass" <<
			\global { \removeWithTag #'sheet \unfoldRepeats \articulate \lower }
		>>
		\new Dynamics = "Pedal_pf" \pedal
	>>

	\midi {}
}

#(define ac:normalFactor '(1 . 1))
\score {
	\new PianoStaff = "PianoStaff_pf" <<
		\new Staff = "Staff_pfTreble" <<
			\global { \removeWithTag #'sheet \unfoldRepeats \articulate \upper }
		>>
		\new Staff = "Staff_pfBass" <<
			\global { \removeWithTag #'sheet \unfoldRepeats \articulate \lower }
		>>
		\new Dynamics = "Pedal_pf" \pedal
	>>

	\midi {}
}
